package main;

public class BilletTrain {

    private float distance;
    
    public static final float[] DISTANCE = {2F, 25F, 50F, 100F, 150F, 300F, Float.MAX_VALUE};
    public static final int[] BAREMEBORD = {10, 15, 25, 35, 60, 90};
    public static final int[] BAREMECONTROLE = {50, 50, 50, 50, 90, 120};



    public int getTarif(boolean onBoard) {
        if (onBoard) {
            return getTarifAvecTable(BAREMEBORD);
        }
        return getTarifAvecTable(BAREMECONTROLE);
    }

    
    private int getTarifAvecTable(int[] tableTarif) {
        int categorieDistance = 1;
        
        while(distance > DISTANCE[categorieDistance]) {
            categorieDistance++;
        }
        
        return tableTarif[categorieDistance - 1];
    }
    

    public BilletTrain(float distance) {
        if (distance <= DISTANCE[0]) {
            throw new IllegalArgumentException();
        }
        this.distance = distance;
    }

}
