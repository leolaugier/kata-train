package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import main.BilletTrain;


public class TestBilletTrain {
    
    @Test (expected=IllegalArgumentException.class)
    public void testDistanceParcourueAuMoins2km() {
        BilletTrain sncf = new BilletTrain(2F);
    }
    
    @Test
    public void testTarifBar�meBordPlusde2kms() {
        BilletTrain sncf = new BilletTrain(2.5F);
        assertEquals(10,sncf.getTarif(true));
    }
    
    @Test
    public void testTarifBar�meContr�lePlusde2kms() {
        BilletTrain sncf = new BilletTrain(2.5F);
        assertEquals(50,sncf.getTarif(false));
    }
    
    @Test
    public void testTarifBar�meBordPlusde25kms() {
        BilletTrain sncf = new BilletTrain(26F);
        assertEquals(15,sncf.getTarif(true));
    }
    
    @Test
    public void testTarifBar�meContr�lePlusde25kms() {
        BilletTrain sncf = new BilletTrain(26F);
        assertEquals(50,sncf.getTarif(false));
    }
    
    @Test
    public void testTarifBar�meBordPlusde50kms() {
        BilletTrain sncf = new BilletTrain(51F);
        assertEquals(25,sncf.getTarif(true));
    }
    
    @Test
    public void testTarifBar�meContr�lePlusde50kms() {
        BilletTrain sncf = new BilletTrain(51F);
        assertEquals(50,sncf.getTarif(false));
    }
    
    @Test
    public void testTarifBar�meBordPlusde100kms() {
        BilletTrain sncf = new BilletTrain(101F);
        assertEquals(35,sncf.getTarif(true));
    }
    
    @Test
    public void testTarifBar�meContr�lePlusde100kms() {
        BilletTrain sncf = new BilletTrain(101F);
        assertEquals(50,sncf.getTarif(false));
    }
    
    @Test
    public void testTarifBar�meBordPlusde150kms() {
        BilletTrain sncf = new BilletTrain(151F);
        assertEquals(60,sncf.getTarif(true));
    }
    
    @Test
    public void testTarifBar�meContr�lePlusde150kms() {
        BilletTrain sncf = new BilletTrain(151F);
        assertEquals(90,sncf.getTarif(false));
    }
    
    @Test
    public void testTarifBar�meBordPlusde300kms() {
        BilletTrain sncf = new BilletTrain(301F);
        assertEquals(90,sncf.getTarif(true));
    }
    
    @Test
    public void testTarifBar�meContr�lePlusde300kms() {
        BilletTrain sncf = new BilletTrain(301F);
        assertEquals(120,sncf.getTarif(false));
    }
}